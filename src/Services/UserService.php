<?php
namespace Digitall\AioraUserService\Services;

use Ellaisys\Cognito\AwsCognitoClient;
use \Illuminate\Support\Collection;

class UserService
{

    private $client;
    /**
     * UserService constructor.
     */
    public function __construct(AwsCognitoClient $client)
    {
        $this->client = $client;
    }

    public function register(Collection $data) : bool
    {
        $cognitoRegistered=$this->client->inviteUser(
            $data['username'] ,
            $data['password'],
            ['email' => $data['email']]
        );
        //TODO: save user to our database

        return $cognitoRegistered;
    }

    public function login(Collection $data)
    {
         return  $this->client->authenticate($data['username'],$data['password']);
    }

    public function passwordReset(Collection $data)
    {
        return $this->client->setUserPassword($data['username'] , $data['password']);
    }
    public function getUser($jwt)
    {
        return $this->client->getUserByAccessToken($jwt);
    }
}
