<?php
namespace Digitall\AioraUserService\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Routing\Controller;
use Digitall\AioraUserService\Http\Requests\CreateUserRequest;
use Digitall\AioraUserService\Services\UserService;

class UserController extends Controller
{
    private $UserService;
    /**
     * UserController constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->UserService = $userService;
    }

    public function register(CreateUserRequest $request)
    {
        $collection = collect($request->all());
        $data = $collection->only('username', 'email', 'password');

        $response = $this->UserService->register($data);
        return response()->json($response);
    }

    public function login(Request $request)
    {
        $collection = collect($request->all());
        $data = $collection->only( 'username', 'password');

        $response = $this->UserService->login($data);
        return response()->json($response);
    }
    public function passwordReset(Request $request)
    {
        $collection = collect($request->all());
        $data = $collection->only( 'username', 'password');

        $response = $this->UserService->passwordReset($data);
        return response()->json($response);
    }

    public function index(){
        return response()->json(['data' => 'User data']);
    }

}
