<?php
namespace Digitall\AioraUserService\Http\Requests;


class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255',
            'email' => 'required|email|max:64|unique:users',
            'password' => 'min:6|max:64',
        ];
    }

}
