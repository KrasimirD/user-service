<?php
namespace Digitall\AioraUserService\Http\Middlewares;

use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Closure;
use Illuminate\Http\Request;
use Digitall\AioraUserService\Services\UserService;

class AuthMiddleware
{


    private $userService;
    /**
     * AuthMiddleware constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function handle($request, Closure $next)
    {
        /**
         * @var Request $request
         */
        try{
            $user = $this->userService->getUser($request->header('Authorization'));

        }catch (CognitoIdentityProviderException $exception){
            return response(["error" => 'Invalid or Missing Jwt Token'], 403);
        }

        return $next($request);
    }
}
