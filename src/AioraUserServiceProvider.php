<?php
namespace Digitall\AioraUserService;

use Illuminate\Support\ServiceProvider;
use Digitall\AioraUserService\Http\Middlewares\AuthMiddleware;


class AioraUserServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/aiora_user_service.php' => config_path("aiora_user_service.php"),
        ],"aiora-config");

        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        /** @var Router $router */
        $router = $this->app['router'];
        $router->aliasMiddleware('authMiddleware', AuthMiddleware::class);
    }

    public function register()
    {

    }
}
