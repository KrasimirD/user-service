<?php

use Illuminate\Support\Facades\Route;
use Digitall\AioraUserService\Http\Controllers\UserController;


Route::group(['middleware' => 'authMiddleware'], function () {
    Route::get('user' , [UserController::class, 'index'])->name("index");
});


Route::post('user' , [UserController::class, 'register'])->name("register");
Route::post('login' , [UserController::class, 'login'])->name("login");
Route::post('passwordReset' , [UserController::class, 'passwordReset'])->name("passwordReset");
